Readme file
********************************
Author: Vamshikrishna N R
date: 31-01-2024
version: v1.0.0
description: To create EKS cluster with 2 worker nodes in private subnets,
VPC with 2 public subnets and private. out of 2 available zones
in private subnets only one NAT gateway attached and associated to 
public subnet and Internet Gateway IGW for two public subnets.
you can access the application with classical load balancer DNS.

**note: since its a basic application i have used clasical loadbalancer,
instead of ingress controller due time constraint and my busy schedule
             

***********************
Pre-requisites

	1.install kubectl
	2.install aws cli
	3.install terraform
      4.connect to AWSconsole via aws-config
     

*****************

1. goto the folder where you have .tf filesin you laptop and performed terraform commands below
	"terraform init"
	"terraform plan"
	"terraform apply"      -yes

note*: give some time/wait AND refresh the AWS console  and in browser

2.after cluster creation update the kubecofig in your local  using command below
   "aws eks update-kubeconfig --region ap-south-1 --name demo-cluster"

3. enter below cammand to deploy application in cluster with below command
	1.kubctl apply -f eks_vpc.yaml
    2.kubectl get pods -o wide
	
4. goto loadbalancer and copy the "dns url" and paste it in any of your browser

you will see " Hello this from aws-eks"

thanks
Vamshi
************************************************

      


