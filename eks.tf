module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.29.0"
  
  cluster_name = "demo-cluster"
  cluster_version = "1.25"

  subnet_ids = module.vpc.private_subnets
  vpc_id = module.vpc.vpc_id

  tags = {
    Environment = "dev"
    application = "demo-app"
  }
    eks_managed_node_groups = {
        dev = {
            min_size = 1
            max_size = 3
            desired_size = 2

            instance_types = ["t2.medium"]
        }
    }

}